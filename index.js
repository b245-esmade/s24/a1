


//1-4

let getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}.`);



//5-6

let address = ["1144N", "Dugay Rd Ext.", "Mahabang Parang ", "Binangonan", "Rizal", "1940"];
let [houseNumber, street, barangay, municipality, province, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${barangay} ${municipality} ${province} ${zipCode}`);



// 7-8

let animal = { 
	name:"Lolong" ,
	species:"crocodile", 
	weight:"1075kgs", 
	measurement:"20ft 3in"}

let {name,species,weight,measurement} = animal

console.log(`${name} was a saltwater ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);



// 9-10

let numbers = [1,2,3,4,5];

numbers.forEach((num) =>
	console.log(num));




// 11

const reduceNumber = numbers.reduce( (a,b) => a + b ); 
	console.log(reduceNumber);



// 12-13

	class Dog{
		constructor(name,age,breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;
		}
	}

let myDog = new Dog("Frankie",5,"Miniature Dachshund");
console.log(myDog);